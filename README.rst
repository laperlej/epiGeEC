============
epiGeEC
============

The **epiGenomic Efficient Correlator** tool is designed to efficiently perform pairwise correlations of thousands of epigenomic datasets. It supports a growing number of file formats and offers the possibility to compute correlations at any resolution on costum or predefined filtered regions.
    
A galaxy implementation including thousands of pre-computed public datasets is availalble at http://epigeec.genap.ca/galaxy/ and also includes support for the WIG format and Spearman correlation(coming soon to command line version). It also offers tools for further annotation and analysis of the matrix files created by epiGeEC.

For more information see the official repository for the epiGeEC tool at https://bitbucket.org/labjacquespe/epigeec
  